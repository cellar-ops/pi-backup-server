# pi-backup-server

Incremental backups, regular hard drive health checks, etc. Suited for a Pi Zero W stuck to the side of a USB drive hidden in your basement, garage, friend's house, etc.

## Definitions

BIP: Pi Backup

## The Plan

Daily:
* BIP mounts the USB HD
* BIP checks USB HD health report
  * If HEALTHY
    * Connect to target system to backup #1
    * Get last HD SMART health log
      * If HEALTHY
        * Rsync backup
      * If OMG
        * Email, flash lights, etc. Maybe send SMS
    * Proceed to target system #2
  * If OMG
    * Email, flash RED lights, turn on batsignal
* BIP unmounts the USB HD

Even months:
* BIP runs a short SMART test on the USB HD

Odd months:
* BIP runs a long SMART test on the USB HD

## Initial design idea

A single Bash or Perl script for all logic put on a nightly cron. It then decides which action to take. If it's a test night, perform the backup first, then initiate long/short test.

## Pains/Open Questions

Smartmontools
* smartctl can be a pain to set up as a user, and may be better self-contained as a root-based drive health check, separate from the backup script.
* The backup script could just check the last health result before proceeding with backups.
* Ideally backups won't run if a test is in progress, or will be deferred?
* If the backup script is handling the health check, it's trivial to make the SMART test run in the foreground, then run backups when it finishes [described here](https://www.thomas-krenn.com/en/wiki/SMART_tests_with_smartctl#Long_Test)
  * But is this necessary? Is there any risk running backups while a test is running?

* Based on [this](https://superuser.com/a/868817) I'm inclined to set up smartd for scheduled tests separate from backups. It's reported there is no performance loss during tests.
